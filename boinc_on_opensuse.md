---
title: 'Quick start boinc on openSUSE Tumbleweed for Rosetta@home'
author:
    - Paul Gonin
geometry: margin=1in
header-includes: |
    \usepackage{graphicx}
    \usepackage{titling}
    \usepackage{fancyhdr}
    \pagestyle{fancyplain}
    \fancyhead{}
    \rhead{\fancyplain{}{\thetitle}}
    \lfoot{\raisebox{-0.5\height}{\includegraphics[width=1in]{pdf/suse-logo.png}}}
    \cfoot{}
    \rfoot{\thepage}

---

# Create account and collect keys
First step is to create an account 'for command line users' on
https://boinc.bakerlab.org/rosetta//create_account_form.php?next_url=

On the account page on [Rosetta account page creation](https://boinc.bakerlab.org/rosetta/home.php), you can collect the key to be used. Click on [View](https://boinc.bakerlab.org/rosetta/weak_auth.php) next to Account keys and keep a note of your *weak* account key.

# Install boinc
Then, you need to install boinc which is available for openSUSE tumbleweed

```bash
sudo zypper in boinc-client
```

Once it is installed you need to start and enable the boinc service (standard systemd)
```bash
sudo systemctl start boinc-client.service
sudo systemctl enable boinc-client.service
```

You can check it is properly started with
```bash
sudo systemctl status  boinc-client.service
```

# Attach boinc to Rosetta@Home project
Once the boinc client service is started you can proceed with attaching it to the Rosetta@Home project.
It is important to execute the boinccmd command from the boinc directory */var/lib/boinc*
(certainly as root)

```
cd /var/lib/boinc
boinccmd --project_attach http://boinc.bakerlab.org/rosetta/ $your_weak_key$
```

You can check the status and progress with get_project_status and get_tasks commands
```
boinccmd --get_project_status

boinccmd --get_tasks
```
